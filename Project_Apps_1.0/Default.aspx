﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Project_Apps_1._0._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
           <!DOCTYPE html>

    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title></title>
        <!-- OPTIONAL SCRIPTS -->
        <%: Styles.Render("~/bundles/form/css") %>        
        
        <link rel="stylesheet" href="dist/css/adminlte.min.css">
        <!-- jQuery -->
        <script src="plugins/jquery/jquery.min.js"></script>
        <script src="plugins/chart.js/Chart.min.js"></script>
        <script src="dist/js/demo.js"></script>
        <script src="dist/js/pages/dashboard3.js"></script>
        <%: Scripts.Render("~/bundles/form/js") %>        
    </head>
    <body>
                
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <div class="content-header">
                    <div class="container-fluid">
                        <div class="row mb-2">
                            <div class="col-sm-6">
                            <h1 class="m-0 text-dark">Home</h1>
                            </div><!-- /.col -->
                            <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="#">Home</a></li>                                
                            </ol>
                            </div><!-- /.col -->
                        </div><!-- /.row -->
                    </div><!-- /.container-fluid -->
                </div>
                <!-- /.content-header -->

                <!-- Main content -->
                <div class="content">
                    <div class="container-fluid">
                        <form id="formMenu"  runat="server" role="form" method="post" enctype="multipart/form-data">
                            <div class="card-body">
                                <div class="row">
                                  <div class="col-md-6">
                                      <label>Minimal</label>
                                      <select class="form-control select2" style="width: 100%;">
                                        <option selected="selected">Alabama</option>
                                        <option>Alaska</option>
                                        <option>California</option>
                                        <option>Delaware</option>
                                        <option>Tennessee</option>
                                        <option>Texas</option>
                                        <option>Washington</option>
                                      </select>
                                    </div>
                                    <div class="col-md-6">
                                        *Data Statis 
                                    </div>
                                  </div>
                                <div class="row">
                                  <div class="col-md-6">
                                      <label>Minimal</label>
                                      <select class="form-control select2 cb-usergroup" style="width: 100%;">
                                        <option selected=""></option>                                        
                                      </select>
                                   </div>
                                    <div class="col-md-6">
                                        *Data Dinamis dari Database
                                    </div>
                                 </div>
                            </div>
                          </form>
                    </div>
                    <!-- /.container-fluid -->
                </div>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->                    
    </body>
        <script src="apps/pages/home/home.js"></script>
    </html>
</asp:Content>
