﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;

namespace Project_Apps_1._0
{
    public partial class _Default : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {            
        }

        private DataTable getDataPeserta()
        {
            try
            {
                string connectionString = ConfigurationManager.ConnectionStrings["config_db"].ConnectionString;
                SqlConnection koneksi = new SqlConnection(connectionString);
                koneksi.Open();
                SqlCommand command = new SqlCommand();
                command.Connection = koneksi;
                command.CommandText = "select * from MS_MENU";
                SqlDataReader reader = null;
                reader = command.ExecuteReader();

                DataTable dt = new DataTable();
                dt.Load(reader);
                koneksi.Close();
                return dt;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        private void fillgvPeserta()
        {
            try
            {
                //pertama-tama kita ambil data dari database dulu ya, terus ditampilkan ke grid view
                DataTable dt = this.getDataPeserta();

                //menampilkan ke gridview
                
            }
            catch (Exception ex)
            {
                
            }
        }


    }
}