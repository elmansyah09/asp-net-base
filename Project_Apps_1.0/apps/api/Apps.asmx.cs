﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using WebServiceHelpers = Project_Apps_1._0.App_Code.Helpers.WebServiceHelpers;
using GlobalHelpers = Project_Apps_1._0.App_Code.Helpers.GlobalHelpers;
using ModelHelpers = Project_Apps_1._0.App_Code.Helpers.ModelHelpers;


namespace Project_Apps_1._0.apps.api
{
   
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    //[ScriptService]    
    public class Apps : System.Web.Services.WebService
    {
        public string conStr = ConfigurationManager.ConnectionStrings["config_db"].ConnectionString;

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void getMenuList()
        {
            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(conStr))
            {
                string sql = string.Format(@"Select * from [MS_MENU]");
                SqlDataAdapter da = new SqlDataAdapter(sql, conn);
                da.Fill(dt);
            }
            //var lst = dt.AsEnumerable().ToList();  
            var lst = dt.AsEnumerable()
                              .Select(r => r.Table.Columns.Cast<DataColumn>()
                                      .Select(c => new KeyValuePair<string, object>(c.ColumnName, r[c.Ordinal])
                                     ).ToDictionary(z => z.Key, z => z.Value)
                              ).ToList();

            //now serialize it  
            JavaScriptSerializer js = new JavaScriptSerializer();
            Context.Response.Write(js.Serialize(lst));

        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void login()
        {
            
            dynamic stuff = WebServiceHelpers.requestForm();
            string username = stuff.email;
            string pass = stuff.password;
            Boolean status = false;
            string res = "";

            DataTable dt = new DataTable();
            using (SqlConnection conn = new SqlConnection(conStr))
            {
                conn.Open();
                pass = GlobalHelpers.md5(pass);
                string qry = "select * from USERS where USER_NAME='" + username + "' and PASSWORD='" + pass + "' and STATUS = 1";
                SqlCommand cmd = new SqlCommand(qry, conn);
                SqlDataReader sdr = cmd.ExecuteReader();
                if (sdr.Read())
                {
                    status = true;
                }
                conn.Close();
            }

            if (status)
            {
                res = WebServiceHelpers.getResponse(200, true, "Berhasil Login", pass);
            } else {
                res = WebServiceHelpers.getResponse(200, false, "Gagal Login", pass);
            }

            Context.Response.Write(res);

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void geUsergroup()
        {

            ModelHelpers db = new ModelHelpers(conStr);
            dynamic data;
            dynamic res;
            Boolean state;
            try
            {
                data = db.getSelectSimple(@"Select * from [USERGROUP]");                
                state = true;
            }
            catch (Exception e)
            {
                state = false;
                data = e.ToString();
            }                        

            if (state)
            {
                res = WebServiceHelpers.getResponse(200, true, "Data Usergroup", data);
            }
            else
            {
                res = WebServiceHelpers.getResponse(200, false, "Gagal Request Usergroup",data);
            }

            Context.Response.Write(res);


        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public void addGroup()
        {

            List<dynamic> param = ModelHelpers.paramsWeb();
            

            ModelHelpers db = new ModelHelpers(conStr);
            dynamic data;
            dynamic res;
            Boolean state;
            try
            {
                data = db.executeInsert("USERGROUP",param);
                state = true;
            }
            catch (Exception e)
            {
                state = false;
                data = e.ToString();
            }

            if (state)
            {
                res = WebServiceHelpers.getResponse(200, true, "Data Usergroup", data);
            }
            else
            {
                res = WebServiceHelpers.getResponse(200, false, "Gagal Request Usergroup", data);
            }

            Context.Response.Write(res);


        }

    }

    
}
