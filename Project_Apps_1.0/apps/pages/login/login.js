﻿$jefR("login_page", {
    init: function () {
        var me = this;
        $jefR.apps(me, {

            controller: {
                init() {                    
                    $("#btn-action-signin").on("click", function () {
                        me.actionLogin();
                    })
                },
                actionLogin() {
                    let formLogin = $("#form-login");
                    let params = me.application.helpers.convert_form(formLogin.serializeArray())
                    me.application.helpers.requestAjax("/apps/api/Apps.asmx/login", "POST", JSON.stringify(params), function (state, result) {
                        if (state) {                            
                            if (result.status) {
                                Swal.fire(
                                    result.message,
                                    '',
                                    'success'
                                );
                                window.location.href = window.location.origin;
                            } else {
                                Swal.fire(
                                    result.message,
                                    '',
                                    'warning'
                                )
                            }             
                            
                        }
                    })
                }
            }
        });
    },
}).module(function (_me, apps) {

    $(document).ready(function () {

        _me.init();

    });
});
