var _xy_z_ = {
	requestAjax: function (url, method, args, callback, arg_async) {
		var asyncroun = true;
		try {
			if (arg_async === false) {
				asyncroun = false;
			}
		} catch (e) {
			asyncroun = true;
		}

		$.ajax({
				url: url,
				type: method,				
                dataType: "json",
				//async: asyncroun,

				data: args,
				beforeSend: function (xhr) {
					
				},
			})
			.done(function (result) {
				if (callback) {
					callback(true,result);
				}
			})
			.fail(function (result) {
				Swal.fire({
  					type: 'error',
					title: "Informastion",
					text: '(' + result.status + ') ' + result.statusText,					
				});
				callback(false,null);
			})
			.always(function () {
				
			});
	},
	createSimpleList(cmp, link, args, config, callback) {
		$(cmp).select2({
			placeholder: 'Loading...'
		});

		try {
			var can_clear = config.can_clear;
		} catch (e) {
			var can_clear = false;
		}

		try {
			var placeholder = config.placeholder;
		} catch (e) {
			var placeholder = '';
		}

		try {
			var display_value = config.display_value;
		} catch (e) {
			var display_value = 'text';
		}

		try {
			var value = config.value;
		} catch (e) {
			var value = 'id';
		}

		$.ajax({
			url: link,
			method: 'POST',
			dataType: 'json',						
			//async: asyncroun,
			data: args,
			beforeSend: function (xhr) {

			},			
		})
		.done(function (result) {
			$(cmp).html('');
			$(cmp).select2({
				allowClear: can_clear,
				placeholder: placeholder
			});
			$.each(result.data, function (index, val) {
				var html = '<option value="' + val[value] + '" >' + val[display_value] + '</option>';
				$(cmp).append(html);
			});			

			$(cmp).val('').trigger('change');
			if (callback) {
				callback(result)
			}
		})
		.fail(function () {
			console.log('gagal terhubungan dengan server');
		});
	},
	set_form_value(frm, data, display) {
		$.each(data, function (key, value) {
			value = value || '-';
			value = (value == 'null') ? '-' : value;
			value = (typeof display != 'undefined' && value == '-') ? '' : value;
			var $ctrl = $('[name=' + key + ']', frm);

			switch ($ctrl.attr("type")) {
				case "text":
				case "hidden":
					$ctrl.val(value);
				case "radio":
				case "checkbox":
					$ctrl.attr("checked", true);
					$ctrl.click();
				case "html":
					if ($ctrl.attr("type") == "html") {
						$ctrl.html(value);
					}
					case "select":
						$ctrl.val(value);
					case "file":
						break;
					default:
						$ctrl.val(value);
			}

			if ($(this).is('.input-date')) {
				if (value != null && value != '') {
					value = (isNaN(new Date(value))) ? app.formatDate(value, '/') : value;
					$(this).datepicker('update', new Date(value));
				}
			}

			if ($ctrl.is('.select2:not([multiple])')) {
				$ctrl.val(value).trigger('change.select2');
				$ctrl.val(value).trigger('change');
			}
		});
	},
	convert_form(param) {
		var out = {};
		for (var i = 0; i < param.length; i++) {
			out[param[i].name] = param[i].value;
		}

		return out;
	},
	reset_form_values(form) {		
		$(form).find('input[type=text], input[type=hidden], input[type=search], input[type=number], input[type=password], input[type=date], input[type=email], input[type=file], textarea, select').val('');
		$(form).find('.select2').val('').trigger('change');
		$(form).find('.displayfield').html('');
		$(form).find('.input-date').val('').datepicker('update');
		$(form).find('.error').removeClass('error');
	}
}
$jefR.application().helper(_xy_z_);
var _xy_z_ = "";