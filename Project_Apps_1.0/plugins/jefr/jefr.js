/*!
 * jefR Library v1.8.2
 * Module Library Javascript
 *
 * Copyright 2018
 * Author 	: Elmansyah Fauzi Rachman
 * Date		: 01 Mei 2018
 */
// (function( window, undefined ) {
window.$jefR = window.$;

var jefR = function (selector, context) {
	// The jefR object is actually just the load constructor 'enhanced'		
	return new jefR.fn.load(selector, context);
};


jefR.apps = function (me, context) {
	return new jefR.apply.init(me, context);
}

jefR.apply = jefR.prototype = {

	constructor: jefR.apps,
	init: function (me, context) {
		if (typeof context == 'object') {			
			jefR.fn.load(me.name, context);
			for (var name in jefR.fn.config.controller) {
				if (name in jefR.fn.config) {
					console.log("This name function already definition");
				} else {
					me[name] = jefR.fn.config.controller[name];
				}
			}
			let except = ['config', 'controller', 'init'];
			for (let name in jefR.fn.config) {
				if (except.indexOf(name) === -1) {
					me[name] = jefR.fn.config[name];
				}
			}
			delete me.init;
			//me.controller = jefR.fn.config.controller;
		}
	},
	commit: function() {
		
	}
}

jefR.commit = function(me) {
	jefR(me.name,me);
}

jefR.fn = jefR.prototype = {
	constructor: jefR,
	application: {},
	helpers: {},
	config: {},
	modules: [],
	load: function (selector, context) {

		if (!selector) {
			return this;
		}

		if (selector && selector != null && selector != '') {
			if (context && typeof context == 'object') {

			} else {
				let module = jefR.fn.modules.filter(function (row) {
					return row.name == selector;
				})
				if (module.length > 0) {
					delete module[0].controller;
					return module[0];
				}
			}
		}

		let config = Object.assign({}, context);

		config.name = selector;
		//config.controller = {};							
		jefR.fn.config = config;
		if (typeof context == 'object') {
			if (!'controller' in context) {
				config.controller = context.controller;
			}
		}

		jefR.fn.application = jefR.config.config;
		jefR.fn.helpers = jefR.config.helpers;
		let allex = [];
		jefR.fn.modules.map(function (row, idx) {
			if (row.name == config.name) {
				allex.push(idx);
			}
		})
		if (allex.length > 0) {
			jefR.fn.modules[allex[0]] = config;
		} else {
			jefR.fn.modules.push(config);
		}

		try {
			if ('init' in context && typeof context.init) {
				var me = Object.assign({}, jefR.fn.config);
				me.application = jefR.fn.application;
				me.application.helpers = jefR.fn.helpers;

				var init = context.init;


				context = me;
				context.init = init;

				context.init();

			}
		} catch (e) {

		}

	},
	module: function (callback) {

		if (typeof callback == 'function') {
			for (var name in jefR.fn.config.controller) {
				if (name in jefR.fn.config) {
					console.info("This name function already definition");
				} else {
					jefR.fn.config[name] = jefR.fn.config.controller[name];
				}
			}
			let apps = jefR.fn.application;
			apps.helpers = jefR.fn.helpers;
			delete jefR.fn.config.controller;
			let a = jefR(jefR.fn.config.name);			
			callback(a, apps);
		} else {
			console.info("You must add config load!");
		}

		
	},
}

jefR.application = function (context) {
	return new jefR.config.init(context);
}

jefR.config = jefR.prototype = {

	constructor: jefR.application,
	config: {},
	helpers: {},
	init: function (context) {

		if (typeof context == 'object') {
			for (var rec in context) {

				jefR.config.config[rec] = context[rec];
			}

		}
	},
	helper: function (context) {
		if (typeof context == 'object') {
			for (var rec in context) {

				jefR.config.helpers[rec] = context[rec];
			}

		}
	}
}

jefR.helpers = function (name, parameter) {
	return new jefR.help.init(name, parameter);
}

jefR.help = jefR.prototype = {

	constructor: jefR.helpers,
	init: function (name, parameter) {
		var helpers = jefR.config.helpers;

		if (typeof helpers == 'object') {
			try {
				var par = parameter.length;
			} catch (e) {
				var par = false;
				console.error("jefR:", "Parameter 2 is not array []");
			}

			if (par !== false && name in helpers) {
				if (typeof helpers[name] == "function") {
					var func = helpers[name],
						nullFunc = Function.prototype.bind.apply(
							func, [null].concat(parameter));
					return new nullFunc();
				}
			}
		}

	}
}


jefR.fn.load.prototype = jefR.fn;
jefR.config.init.prototype = jefR.config;
jefR.help.init.prototype = jefR.help;
jefR.apply.init.prototype = jefR.apps;
window.$jefR = jefR;
delete window.jefR;
delete jefR;
// });