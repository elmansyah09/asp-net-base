﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Script.Serialization;

namespace Project_Apps_1._0.App_Code.Helpers
{
    public class WebServiceHelpers
    {
        public int statusCode
        {
            get;
            set;
        }
        public Boolean status
        {
            get;
            set;
        }

        public string message
        {
            get;
            set;
        }
        public dynamic data
        {
            get;
            set;
        }

        public static dynamic requestForm()
        {
            string json;
            using (var reader = new StreamReader(HttpContext.Current.Request.InputStream))
            {
                json = reader.ReadToEnd();
            }
            var person = Json.Decode(json);
            dynamic stuff = JsonConvert.DeserializeObject(json);

            return stuff;
        }
        
        public static string getResponse(int status_code, Boolean state, String msg, dynamic data)
        {
            WebServiceHelpers res = new WebServiceHelpers()
            {
                statusCode = status_code,
                status = state,
                message = msg,
                data = data
            };
            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(res);
        }

        public static string getResponse(int status_code, Boolean state, String msg)
        {
            WebServiceHelpers res = new WebServiceHelpers()
            {
                statusCode = status_code,
                status = state,
                message = msg                
            };
            JavaScriptSerializer js = new JavaScriptSerializer();
            return js.Serialize(res);
        }

    }

   
}