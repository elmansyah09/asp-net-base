﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Project_Apps_1._0.App_Code.Helpers
{
    public class ModelHelpers
    {
        
        public String conStr;

        public ModelHelpers(string conStr)
        {
            this.conStr = conStr;
        }


        public dynamic getSelectSimple(string query)
        {
            try
            {
                DataTable dt = new DataTable();
                using (SqlConnection conn = new SqlConnection(conStr))
                {
                    string sql = string.Format(query);
                    SqlDataAdapter da = new SqlDataAdapter(sql, conn);
                    da.Fill(dt);
                }
                //var lst = dt.AsEnumerable().ToList();  
                var lst = dt.AsEnumerable()
                                  .Select(r => r.Table.Columns.Cast<DataColumn>()
                                          .Select(c => new KeyValuePair<string, object>(c.ColumnName, r[c.Ordinal])
                                         ).ToDictionary(z => z.Key, z => z.Value)
                                  ).ToList();                
                return lst;
            }
            catch (Exception e)
            {
                return e;
            }
            
        }

        public dynamic executeInsert(string table_name, List<dynamic> args)
        {
            try
            {
                string[] fieldName = new string[args.Count];
                int countIndex = 0;
                foreach (ParamsMethod items in args)
                {
                    fieldName[countIndex] = items.name;
                    countIndex++;
                }

               return fieldName;
            }
            catch (Exception e)
            {
                return e;
            }

        }


        public static dynamic paramsWeb()
        {
            dynamic par = new List<ParamsMethod>();
            par.Add(ModelHelpers.addParams("USERGROUP_ID", 3));
            par.Add(ModelHelpers.addParams("USERGROUP_NAME", "User"));
            par.Add(ModelHelpers.addParams("STATUS", 1));
            return par;
        }

        public static dynamic addParams(string name, dynamic value)
        {
            ParamsMethod data = new ParamsMethod(name, value);
            return data;
        }

        class ParamsMethod
        {

            public string name { get; set; }
            public dynamic value { get; set; }

            public ParamsMethod()
            {

            }

            public ParamsMethod(string name, dynamic value)
            {
                this.name = name;
                this.value = value;
            }


        }
    }
}